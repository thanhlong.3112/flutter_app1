import 'dart:convert';

class Movie {
  final bool adult;
  final String backdrop_path;
  final List genre_ids;
  final int id;
  final String original_language;
  final String original_title;
  final String overview;
  final double popularity;
  final String poster_path;
  final String release_date;
  final bool video;
  final double vote_average;
  final int vote_count;
  final String title;

  Movie(
      {required this.adult,
      required this.backdrop_path,
      required this.genre_ids,
      required this.id,
      required this.original_language,
      required this.overview,
      required this.original_title,
      required this.popularity,
      required this.video,
      required this.release_date,
      required this.poster_path,
      required this.title,
      required this.vote_average,
      required this.vote_count});

  factory Movie.fromJson(Map<String, dynamic> json) {
    return Movie(
        adult: json['adult'],
        backdrop_path: json['backdrop_path'],
        genre_ids: json['genre_ids'],
        id: json['id'],
        original_language: json['original_language'],
        original_title: json['original_title'],
        overview: json['overview'],
        popularity: json['popularity'],
        poster_path: json['poster_path'],
        release_date: json['release_date'],
        video: json['video'],
        vote_average: json['vote_average'],
        vote_count: json['vote_count'],
        title: json['title']);
  }
}
