// import 'dart:convert';
// import 'dart:async';

// import 'package:flutter/material.dart';
// import 'package:http/http.dart' as http;

// import 'movie.dart';

// class MovieProvider extends ChangeNotifier {
//   List<Movie> _movies = [];
//   List<Movie> get movies {
//     return [..._movies];
//   }

//   Future<List<Movie>> fetchAllMovies(BuildContext context) async {
//     final response = await http.get(Uri.parse(
//         'https://api.themoviedb.org/3/movie/popular?api_key=65302d9fd57dda3ea2ba86f370ab6b7f&language=en-US&page=1'));

//     if (response.statusCode == 200) {
//       final jsonReponse = json.decode(response.body);
//       Iterable item = jsonReponse['results'] as List<dynamic>;
//       return item.map((json) => Movie.fromMap(json)).toList();
//     } else {
//       throw Exception("Failed to load movies!");
//     }
//   }
// }
