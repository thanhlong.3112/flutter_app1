import 'package:flutter/material.dart';
import 'package:flutter_app1/screen/detail_movie_screen.dart';

import '../movie.dart';

class MoviesWidget extends StatelessWidget {
  final List<Movie> movies;

  MoviesWidget({required this.movies});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Movie List'),
        ),
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: GridView.builder(
              itemCount: movies.length,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                mainAxisSpacing: 20,
                crossAxisSpacing: 20,
                childAspectRatio: 3 / 2,
              ),
              itemBuilder: (context, index) {
                final items = movies[index];
                return ClipRRect(
                  borderRadius: BorderRadius.circular(12),
                  child: GridTile(
                    child: GestureDetector(
                      onTap: () {
                        Navigator.pushNamed(
                            context, DetailMovieScreen.routeName,
                            arguments: items);
                      },
                      child: Image(
                        image: NetworkImage(
                            'https://image.tmdb.org/t/p/w500${items.poster_path}'),
                        fit: BoxFit.cover,
                      ),
                    ),
                    footer: GridTileBar(
                      backgroundColor: Colors.black87,
                      title: Text(
                        items.title,
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                );
              }),
        ),
      ),
    );
  }
}
