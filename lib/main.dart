import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_app1/screen/detail_movie_screen.dart';
import 'package:flutter_app1/screen/movie_list_screen.dart';
import 'package:http/http.dart' as http;

import 'movie.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  List<Movie> _movies = [];

  @override
  void initState() {
    super.initState();
    _getMovies();
  }

  void _getMovies() async {
    final movies = await getData();
    setState(() {
      _movies = movies;
    });
  }

  Future<List<Movie>> getData() async {
    final response = await http.get(Uri.parse(
        'https://api.themoviedb.org/3/movie/popular?api_key=65302d9fd57dda3ea2ba86f370ab6b7f&language=en-US&page=1'));

    if (response.statusCode == 200) {
      final jsonReponse = json.decode(response.body);
      Iterable item = jsonReponse['results'] as List<dynamic>;
      return item.map((json) => Movie.fromJson(json)).toList();
    } else {
      throw Exception("Failed to load movies!");
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: "Movies App",
        onGenerateRoute: generateRoute, // generate router
        onUnknownRoute: (settings) => MaterialPageRoute(
              builder: (context) => const Text('Unknown route'),
            ),
        home: MoviesWidget(movies: _movies));
  }

  Route<dynamic>? generateRoute(RouteSettings settings) {
    if (settings.name == DetailMovieScreen.routeName) {
      return MaterialPageRoute(
          builder: (_) {
            //some custom code
            return const DetailMovieScreen();
          },
          settings: settings);
    }
    return null;
  }
}
